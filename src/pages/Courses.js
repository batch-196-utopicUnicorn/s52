import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard'

export default function Courses(){
	//console.log(coursesData)
	//console.log(coursesData[0])

	const courses = coursesData.map(kors => {
		console.log(kors)
		return(
			<CourseCard key={kors.id} courseProp = {kors}/>

		)

	})

	return(

		<>

			<h1>Available Courses:</h1>
			{courses}
			
		</>
	)
}