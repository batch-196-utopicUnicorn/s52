import {Navigate} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'

export default function Login(){

	const {user, setUser} = useContext(UserContext)
	console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isLoggedIn, setLoggedIn] = useState(false)

	

	function loginUser(e){
		//prevents page redirection via form submission
		e.preventDefault();

		//Set the email of the authenticated user in the local storage
			//Syntax:
				//localStorage.setItem('propertyName/Key', value)
		localStorage.setItem('email', email)

		// Access user information through localStorage to the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		//When states change components are rerendered and the AppNavbar component will be updated based on the credentials

		setUser({
			email: localStorage.getItem("email")
		})

		//clear input fields
		setEmail('');
		setPassword('');


		alert('Login Sucessful');
	}

	useEffect(() =>{
		//validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && password !== ''){
			setLoggedIn(true)
		} else{
			setLoggedIn(false)
		}

	}, [email, password])

	return(
		(user.email !== null) ?
			<Navigate to ='/courses'/>
		:
		<>
		<h1>Login Here:</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value = {email}
				onChange = {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
				We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value = {password}
				onChange = {e => setPassword(e.target.value)}
				/>
			</Form.Group>
			

		{ isLoggedIn ?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
				Login
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>
				Login
			</Button>
		}

		</Form>
		</>
	)
}
