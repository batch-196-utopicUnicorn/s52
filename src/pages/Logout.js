import {useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'

export default function Logout(){

	const {unsetUser, setUser} = useContext(UserContext)

	//clear the localstorage of the user's information
	unsetUser();
	// localStorage.clear()

	//Placing the 'setUser' function inside of a useEffect is necessary because of updates within ReactJs that a state of another component cannot be updated while trying to render a different component
	useEffect(() => {
		setUser({email: null})
	})

	return(
		<Navigate to="/Login"/>
	)
};