import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Banner({bannerProp}){
if(bannerProp){
	return(
		<Row>
			<Col className = "p-5">
				<h1>404 - Page not found</h1>
				<p>Page could not be found</p>
				<Button variant="primary " as={Link} to='/'>Back Home</Button>
			</Col>
		</Row>
	)
}
	return(
		<Row>
			<Col className = "p-5">
				<h1>B196 Booking App</h1>
				<p>Opportunities for everyone, everywhere!</p>
				<Button variant="primary">Enroll Now</Button>
			</Col>
		</Row>
	)
}
