import {useState} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
/*import Banner from './components/Banner';
import Highlights from './components/Highlights';*/
import Courses from './pages/Courses';
import Error from './pages/Error';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

    // state hook for the user state that defined here for a global scope
    // initialized as an object with properties from the localStorage
    // this will be used to the store the user information and will be used for validting if a user is logged in on the app or not
    const [user, setUser] = useState({
        email: localStorage.getItem('email')
    });

    const unsetUser = () => {
        localStorage.clear();
    };


    return (
        <>
            <UserProvider value={{user, setUser, unsetUser}}>
                <Router>
                    <AppNavbar/>
                    <Container>
                        <Routes>
                            <Route exact path="/" element={<Home/>}/>
                            <Route exact path="/courses" element={<Courses/>}/>
                            <Route exact path="/register" element={<Register/>}/>
                            <Route exact path="/login" element={<Login/>}/>
                            <Route exact path="/logout" element={<Logout/>}/>
                            <Route exact path="*" element={<Error/>}/>
                        </Routes>
                    </Container>
                </Router>
            </UserProvider>
        </>
    )
}

export default App;
